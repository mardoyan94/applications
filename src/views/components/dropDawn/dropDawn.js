import React, { Component } from 'react';
import './dropDawn.css';

export class DropDawn extends Component {

    _renderOptions() {
        return this.props.openDropDawn ?
            <div className={`buttonsContainer ${this.props.background ? '' : 'noBackground'}`}>
                {this.props.items.map((name, i) => (<div
                    onClick={() => {
                        this.props.select(name)
                        this.setState({
                            openDropDawn: false
                        })
                    }}
                    key={i}
                    className="selectContainer buttons">
                    <span>
                        {name}
                    </span>
                </div>))}
            </div> :
            null
    }

    _renderBackground() {
        if (this.props.background) {
            return null
        }
        return this.props.openDropDawn ?
            <div className="dropDownBackground"></div> :
            null
    }

    render() {
        return (
            <div>
                <div className={`selectContainer ${this.props.background ? '' : 'selectContainerUpper'}`}>
                    <span>
                        {this.props.active === null ? this.props.placeholder : this.props.active}
                    </span>
                    <div
                        onClick={() => { this.props.openMenu() }}
                        className="openButton">
                        <img
                            alt="альтернативный текст"
                            src={this.props.openDropDawn ?
                                require('../../../assets/icons/arrowUp.png') :
                                require('../../../assets/icons/arrowDown.png')
                            }
                        />
                    </div>
                </div>
                {this._renderOptions()}
                {this._renderBackground()}
            </div>
        );
    }
}

