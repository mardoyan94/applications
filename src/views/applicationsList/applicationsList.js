import React, { Component } from 'react';
import './applicationsList.css';
import * as moment from 'moment';

import { DropDawn } from '../components'

export class ApplicationsList extends Component {

    projects = ['Все', 'Проект 1', 'Проект 2', 'Проект 3', 'Проект 4', 'Проект 5']


    defaultItems = []

    constructor(props) {
        super(props)
        this.state = {
            items: [],
            dateSort: null,
            urgencySort: null,
            openProjectOptions: false,
            project: 'Все'
        }
    }

    componentDidMount() {
        let data = localStorage.getItem('data')
        data = JSON.parse(data)
        for (let i = 0; i < data.length; i++) {
            data[i].dateObject = moment(data[i].date, 'YYYY-MM-DD').toDate()
            switch (data[i].urgency) {
                case 'Низкая':
                    data[i].urgencyId = 1
                    break;
                case 'Обычная':
                    data[i].urgencyId = 2
                    break;
                case 'Высокая':
                    data[i].urgencyId = 3
                    break;
                default:
                    break;
            }
        }
        this.defaultItems = data
        this.setState({
            items: data
        })

    }

    selectProject(value) {
        this.setState({
            dateSort: null,
            urgencySort: null,
            project: value,
            openProjectOptions: false
        })
        if (value === 'Все') {
            this.setState({
                items: this.defaultItems
            })
        }
        else {
            this.setState({
                items: this.defaultItems.filter((item) => item.project === value)
            })
        }
    }

    sortByUrgency() {
        let items = []
        let urgencySort = null
        if (this.state.urgencySort) {

            urgencySort = false
            items = this.defaultItems.sort(function (a, b) {
                return a.urgencyId - b.urgencyId
            })
        }
        else {
            urgencySort = true
            items = this.defaultItems.sort(function (a, b) {
                return b.urgencyId - a.urgencyId
            })
        }

        this.setState({
            items: items,
            urgencySort: urgencySort,
            dateSort: null
        })
    }

    sortByDate() {
        let items = []
        let dateSort = null
        if (this.state.dateSort) {

            dateSort = false
            items = this.defaultItems.sort(function (a, b) {
                return new Date(a.date) - new Date(b.date);
            })
        }
        else {
            dateSort = true
            items = this.defaultItems.sort(function (a, b) {
                return new Date(b.date) - new Date(a.date);
            })
        }

        this.setState({
            items: items,
            dateSort: dateSort,
            urgencySort: null
        })
    }

    _renderItems() {
        return this.state.items.map((item, i) => (
            <tr key={i}>
                <td>{item.date}</td>
                <td>{item.title}</td>
                <td>{item.description}</td>
                <td>{item.project}</td>
                <td>{item.urgency}</td>
            </tr>
        ))
    }

    _renderArrows(value) {
        if (value === null) {
            return (
                <div className="arrowsContainer">
                    <img
                        alt="альтернативный текст"
                        src={require('../../assets/icons/arrowUp.png')}
                    />
                    <img
                        alt="альтернативный текст"
                        src={require('../../assets/icons/arrowDown.png')}
                    />
                </div>
            )
        }
        else if (value) {
            return (
                <img
                    alt="альтернативный текст"
                    src={require('../../assets/icons/arrowDown.png')}
                />
            )
        }
        else {
            return (
                <img
                    alt="альтернативный текст"
                    src={require('../../assets/icons/arrowUp.png')}
                />
            )
        }
    }


    render() {
        console.log(this.props);
        
        return (
            <div className="listContainer">
                <div className='header'>
                    <span className="title">Зарегистрируйте заявку</span>
                    <DropDawn
                        background={false}
                        openDropDawn={this.state.openProjectOptions}
                        openMenu={() => {
                            this.setState({
                                openProjectOptions: !this.state.openProjectOptions,
                                openUrgencyOptions: false
                            })
                        }}
                        placeholder="Выберите проект..."
                        select={(value) => {
                            this.selectProject(value)
                        }}
                        active={this.state.project}
                        items={this.projects} />
                </div>
                <table>
                    <thead>
                        <tr>
                            <th
                                onClick={() => { this.sortByDate() }}
                                className='thForArrow'>
                                Дата
                            {this._renderArrows(this.state.dateSort)}
                            </th>
                            <th>Заголовок</th>
                            <th>Текст заявки</th>
                            <th>Проект</th>
                            <th
                                onClick={() => { this.sortByUrgency() }}
                                className='thForArrow'
                            >
                                Срочность
                                {this._renderArrows(this.state.urgencySort)}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this._renderItems()}
                    </tbody>
                </table>

            </div>
        );
    }
}

