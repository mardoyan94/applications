import React, { Component } from 'react';
import './registerApplication.css';
import { DropDawn } from '../components'

export class RegisterApplication extends Component {

    projects = ['Проект 1', 'Проект 2', 'Проект 3', 'Проект 4']
    urgency = ['Низкая', 'Обычная', 'Высокая']

    constructor(props) {
        super(props)
        this.state = {
            application: {
                title: '',
                description: '',
                date: '',
                project: null,
                urgency: null,
                openProjectOptions: false,
                openUrgencyOptions: false
            }
        }
    }

    applicationRegistered() {
        this.setState({
            application: {
                project: null,
                urgency: null,
                title: '',
                description: '',
                date: '',
                openUrgencyOptions: false
            }
        })
        alert('Ваша заявка зарегистрирована!');
    }

    save() {
        if (this.state.application.title.length < 3) {
            alert('Заголовок слишком короткий!');
        }
        else if (this.state.application.description.length < 10) {
            alert('Текст заявки слишком короткий!');
        }
        else if (this.state.application.project === null) {
            alert('Выберите проект!');
        }
        else if (this.state.application.urgency === null) {
            alert('Выберите срочность!')
        }
        else if (this.state.application.date === '') {
            alert('Выберите дату поступления!');
        }
        else {
            let data = localStorage.getItem('data')
            if (data === null) {
                data = [
                    this.state.application
                ]

                data = JSON.stringify(data)
                localStorage.setItem('data', data)
                this.applicationRegistered()
            }
            else {
                data = JSON.parse(data)
                data.push(this.state.application)
                data = JSON.stringify(data)
                localStorage.setItem('data', data)
                this.applicationRegistered()
            }

        }

    }

    render() {
        return (
            <div className="container">
                <div className="formContainer">
                    <div className='header'>
                    <span className="title">Зарегистрируйте заявку</span>
                    <div 
                    onClick={()=>{ this.props.history.push('/list/1') }}
                    className="openAllButton">
                        <span className='saveButtonText'>
                        ВСЕ ЗАЯВКИ
                        </span>
                    </div>
                    </div>
                    <form>
                        <label className="inputTitles">Заголовок</label>
                        <input
                        value={this.state.application.title}
                            onChange={(evt) => {
                                this.setState({
                                    application:{
                                        ...this.state.application,
                                        title: evt.target.value
                                    }
                                })
                            }}
                            type="text" placeholder="Ваш заголовок..." />
                        <div>
                            <label className="inputTitles">Текст заявки</label>
                            <textarea
                            value={this.state.application.description}
                             rows='100' placeholder="Ваш текст заявки..." 
                            onChange={(evt) => {
                                this.setState({
                                    application:{
                                        ...this.state.application,
                                        description: evt.target.value
                                    }
                                })
                            }}>
                            </textarea>
                        </div>
                        
                        <div className='dropDawnRow'>
                            <div style={{marginRight: '20px'}}>
                        <label className="inputTitles">Проект</label>
                        <DropDawn
                        background={true}
                        openDropDawn={this.state.openProjectOptions}
                          openMenu={()=>{ 
                            this.setState({
                                openProjectOptions: !this.state.openProjectOptions,
                                openUrgencyOptions: false
                            })
                         }}
                            placeholder="Выберите проект..."
                            select={(value) => {
                                this.setState({
                                    openProjectOptions: false,
                                    application:{
                                        ...this.state.application,
                                        project: value
                                    }
                                })
                            }}
                            active={this.state.application.project}
                            items={this.projects} />
                            </div>
                            <div>
                        <label className="inputTitles">Срочность</label>
                        <DropDawn
                        background={true}
                        openDropDawn={this.state.openUrgencyOptions}
                        openMenu={()=>{ 
                            this.setState({
                                openUrgencyOptions: !this.state.openUrgencyOptions,
                                openProjectOptions: false
                            })
                         }}
                            placeholder="Выберите срочность..."
                            select={(value) => {
                                this.setState({
                                    openUrgencyOptions: false,
                                    application:{
                                        ...this.state.application,
                                        urgency: value
                                    }
                                })
                            }}
                            active={this.state.application.urgency}
                            items={this.urgency} />
                            </div>
                            </div>
                        <label className="inputTitles">Дата поступления</label>
                        <input
                            value={this.state.application.date}
                            onChange={(event) => {
                                this.setState({
                                    application:{
                                        ...this.state.application,
                                        date: event.target.value
                                    }
                                })
                            }}
                            className="date"
                            type='date' />
                        <div
                            onClick={() => {
                                this.save()
                            }}
                            className='saveButton'>
                            <span className='saveButtonText'>
                                СОХРАНИТЬ
                                </span>
                        </div>

                    </form>
                </div>
            </div>
        );
    }
}

