import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import React, { Suspense, lazy } from 'react';

const RegisterApplication = lazy(() => import('../views/registerApplication'));
const ApplicationsList = lazy(() => import('../views/applicationsList'));

export const MainRoute = () => (
  <Router>
    <Suspense fallback={<div>Loading...</div>}>
      <Switch>
      <Route exact path="/" component={RegisterApplication}/>
      <Route  path="/list/:id" component={ApplicationsList}/>
        
        
      </Switch>
    </Suspense>
  </Router>
);